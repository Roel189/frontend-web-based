<?php
require_once('functions.php');
index();
?>
<?php include(HEADER_TEMPLATE); ?>

<header>
    <div class="row">
        <div class="col-sm-6">
            <h2>Clientes</h2>
        </div>
        <div class="col-sm-6 text-right h2">
            <a class="btn btn-primary" href="add.php">
                <i class="fa fa-plus"></i>
                Novo Cliente</a>
            <a class="btn btn-default" href="index.php">
                <i class="fa fa-refresh"></i>
                Atualizar
            </a>
        </div>
    </div>
</header>
<?php
if (!empty($_SESSION['message'])) : ?>
    <div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button> <?php echo $_SESSION['message']; ?>
    </div>            <?php clear_messages(); ?><?php endif; ?>
<hr>
<?php $cont_feminino = 0;
      $cont_masculino = 0;?>
    <?php if ($customers) : ?>
        <?php foreach ($customers as $customer) : ?>

           <?php if($customer['sexo']== 'Feminino'){?>
                <?php      $cont_feminino++;?>
           <?php }else {?>
                <?php      $cont_masculino ++ ;?>
          <?php  }?>

        <?php endforeach; ?>
    <?php else : ?>
        <tr>
            <td colspan="6">Nenhum registro encontrado.</td>
        </tr>        <?php endif; ?>        </tbody>

<html>
<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Sexo', 'Quantidade'],
                ['Feminino',     <?php echo $cont_feminino;?>],
                ['Masculino',    <?php echo $cont_masculino;?>],

            ]);

            var options = {
                title: 'Divisão por gênero'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }
    </script>
</head>
<body>
<div id="piechart" style="width: 900px; height: 500px;"></div>
</body>
</html>
<?php include(FOOTER_TEMPLATE); ?>
