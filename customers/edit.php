<?php
require_once('functions.php');
edit();
?>
<?php include(HEADER_TEMPLATE); ?>

    <h2>Atualizar Cliente</h2>

    <form action="edit.php?id=<?php echo $customer['id']; ?>" method="post">
        <hr />
        <div class="row">
            <div class="form-group col-md-7">
                <label for="name">Nome </label>
                <input type="text" class="form-control" name="pessoa['nome']" value="<?php echo $customer['nome']; ?>">
            </div>
                <div class="form-group col-md-3">
                    <label for="campo2"> CPF</label>
                    <input type="text" class="form-control" name="pessoa['cpf']" value="<?php echo $customer['cpf']; ?>">		    </div>
                <div class="form-group col-md-2">
                    <label for="campo3">Data de Nascimento</label>
                    <input type="date" class="form-control" name="pessoa['dataNasc']" value="<?php echo $customer['dataNasc']; ?>">
                </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="campo1">Sexo</label>
                <select  name="pessoa['sexo']" class="selectpicker"  value="<?php echo $customer['sexo']; ?>" >
                    <option   name="customer['sexo']" value="Masculino">Masculino</option>
                    <option  name="customer['sexo']"  value="Feminino">Feminino</option>
                </select>

            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-5">
                <label for="campo1">Endereço</label>
                <input type="text" class="form-control" name="pessoa['endereco']" value="<?php echo $customer['endereco']; ?>">


        <div id="actions" class="row">
            <div class="col-md-12">
                <input type="hidden" name="hidden_framework" id="hidden_framework">
                <button type="submit" class="btn btn-primary">Salvar</button>
                <a href="index.php" class="btn btn-default">Cancelar</a>
            </div>
        </div>

    </form>
    <?php include(FOOTER_TEMPLATE); ?>