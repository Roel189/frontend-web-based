<?php
require_once('functions.php');

?>
<?php include(HEADER_TEMPLATE); ?>

<h2>Pesquisar por Pessoa</h2>
<form action="view_cpf.php?cpf=<?php echo $customer['cpf']; ?>" method="post">
    <!-- area de campos do form -->
    <hr />
    <div class="row">
        <div class="form-group col-md-7">
            <label for="name">Por favor, informe o CPF: </label>
            <input type="text" class="form-control" name="cpf" required>
        </div>
    </div>
        <div id="actions" class="row">
            <div class="col-md-12">
                <input type="hidden" name="hidden_framework" id="hidden_framework">
                <button type="submit" class="btn btn-primary">Concluir</button>
                <a href="index.php" class="btn btn-default">Cancelar</a>
            </div>
        </div>


