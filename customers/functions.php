<?php
require_once('../config.php');
require_once(DBAPI);
$customers = null;
$customer = null;
/**		 *  Listagem de Pessoas		 */
function index() {
    global $customers;
    $customers = find_all('pessoa');
}
function add() {
    if (!empty($_POST['pessoa'])) {
        $customer = $_POST['pessoa'];
        save('pessoa', $customer);
        header('location: index.php');
    }


}
/**		 *	Atualizacao/Edicao de Cliente		 */
	function edit() {
	    if (isset($_GET['id'])) {
	        $id = $_GET['id'];
	        if (isset($_POST['pessoa'])) {
	            $customer = $_POST['pessoa'];
	            update('pessoa', $id, $customer);
	            header('location: index.php');
	        } else {
	            global $customer;
	            $customer = find('pessoa', $id);
	        } 		  } else {
	        header('location: index.php');
	    }
	}
function view($id = null) {
    global $customer;
    $customer = find('pessoa', $id);
}
/*function view_cpf($rg = null) {
    $rg = $_GET['rg'];
    global $customer;
    $customer = find('pessoa', $rg);
}*/
function view_cpf()
{
    global $customer;
    $cpf=$_POST['cpf'];
    $customer = find_cpf('pessoa', $cpf);



}

function delete($id = null) {
    global $customer;
    $customer = remove('pessoa', $id);
    header('location: index.php');
}