<?php
require_once('functions.php');
add();
?>
<?php include(HEADER_TEMPLATE); ?>

    <h2>Cadastro de Pessoas</h2>
    <form action="add.php" method="post">
        <!-- area de campos do form -->
        <hr />
        <div class="row">
            <div class="form-group col-md-7">
                <label for="name">Nome </label>
                <input type="text" class="form-control" name="pessoa['nome']" required>
            </div>
            <div class="form-group col-md-3">
                <label for="campo2">CPF</label>
                <input type="text" class="form-control" name="pessoa['cpf']" required>
            </div>
            <div class="form-group col-md-2">
                <label for="campo3">Data de Nascimento</label>
                <input type="date" class="form-control" name="pessoa['dataNasc']" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-5">
                <label for="campo1">Endereço</label>
                <input type="text" class="form-control" name="pessoa['endereco']">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="campo1">Sexo</label>
                <select  name="pessoa['sexo']" class="selectpicker" required>
                    <option    value=""> </option>
                    <option   name="pessoa['sexo']" value="Masculino">Masculino</option>
                    <option  name="pessoa['sexo']"  value="Feminino">Feminino</option>
                </select>



            </div>
        </div>
        <div id="actions" class="row">
            <div class="col-md-12">
                <input type="hidden" name="hidden_framework" id="hidden_framework">
                <button type="submit" class="btn btn-primary">Salvar</button>
                <a href="index.php" class="btn btn-default">Cancelar</a>
            </div>
        </div>
    </form>
<script>
    $(document).read(function(){
        $('.selectpicker').selectpicker();

        $('#framework').change(function(){
           $('#hidden_framework').val($('#framework').val())
        });
        $('#multiple_select_form').on('submit', function(event){
            event.preventDefault();
            if($('#pessoa[sexo]').val()!='')
            {
                var form_data = $(this).serialize();
                $.ajax({
                    url:".php",
                    method:"POST",
                    data:form_data,
                    sucess:function(data)
                    {
                        //console.log(data);
                        $('#hidden_framework').val('');
                        $('.selectpicker').selectpicker('val', '');
                    }
                })
            }else{
                alert("Selecione o sexo");
                return false;
            }
        });
    });
</script>
<?php include('modal.php'); ?>
<?php include(FOOTER_TEMPLATE); ?>