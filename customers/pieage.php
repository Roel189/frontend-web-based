<?php
require_once('functions.php');
index();
?>
<?php include(HEADER_TEMPLATE); ?>

<header>
    <div class="row">
        <div class="col-sm-6">
            <h2>Gráficos</h2>
        </div>

    </div>
</header>
<?php
if (!empty($_SESSION['message'])) : ?>
    <div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button> <?php echo $_SESSION['message']; ?>
    </div>            <?php clear_messages(); ?><?php endif; ?>

<?php $cont_0a9 = 0;
      $cont_10a19 = 0;
      $cont_20a29 = 0;
      $cont_30a39 = 0;
      $cont_max = 0;
      date_default_timezone_set('America/Sao_Paulo');
      $date = date('Y-m-d '); ?>
<?php if ($customers) : ?>
    <?php foreach ($customers as $customer) : ?>
      <?php  $idade = $date - $customer['dataNasc'];
          ?>
        <?php if($idade<=9){
            $cont_0a9 ++;
        }else if($idade>=10 && $idade<=19){
            $cont_10a19 ++;
        }else if($idade>=20 && $idade<=29){
            $cont_20a29 ++ ;
        }else if($idade>=30 && $idade<=39){
            $cont_30a39 ++ ;
        } else{
            $cont_max ++;
        }?>

    <?php endforeach; ?>
<?php else : ?>
    <tr>
        <td colspan="6">Nenhum registro encontrado.</td>
    </tr>        <?php endif; ?>        </tbody>

<html>
<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Idade', 'Quantidade'],
                ['0 a 9',     <?php echo $cont_0a9;?>],
                ['10 a 19',    <?php echo $cont_10a19;?>],
                ['20 a 29',     <?php echo $cont_20a29;?>],
                ['30 a 39',    <?php echo $cont_30a39;?>],
                ['Maior que 40',     <?php echo $cont_max;?>],
              //  ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"]

            ]);

            var options = {
                title: 'Divisão por idade'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }
    </script>
</head>
<body>
<div id="piechart" style="width: 900px; height: 500px;"></div>
</body>
</html>
<?php include(FOOTER_TEMPLATE); ?>
