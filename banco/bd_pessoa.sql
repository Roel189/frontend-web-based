-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Jan-2018 às 18:50
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_pessoa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `cpf` varchar(13) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `dataNasc` date NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `id` int(11) NOT NULL,
  `bairro` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`cpf`, `nome`, `dataNasc`, `endereco`, `sexo`, `id`, `bairro`) VALUES
('12364111', 'Rodrigo Roel', '1970-12-12', 'Rua das acacias', 'Masculino', 1, ''),
('123.456.789', 'Fulano de Tal', '1989-01-01', 'Rua da Web', 'Masculino', 2, ''),
('897654234', 'pedro', '2000-02-12', 'rua das violetas', 'Masculino', 7, ''),
('02365895', 'isa', '1998-03-31', 'rua das violetas', 'Feminino', 8, ''),
('02365895', 'ana', '2000-09-23', 'rua das violetas', 'Feminino', 9, ''),
('12324548643', 'Victoraaaaaa', '2000-12-13', 'Rua das Araucarias, 201', 'Masculino', 10, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
